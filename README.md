# openwrt-repo

@dfc643 和其朋友修改或创建的一些有用的 OpenWRT 软件包。

## 在更新软件包之前

由于本仓库托管在 GitLab 上，仅支持 HTTPS 协议，所以请确保您的 OpenWRT 设备中安装了以下软件包。

```
opkg install wget ca-certificates
```

## 如何使用？

在 "软件包 > 设置 > 自定义软件源" 中添加如下一行，并 **保存并应用** 相关设置。

```
src/gz xretia_repo https://gitlab.com/dfc643/openwrt-repo/-/raw/master
```

用于联发科 MT762x 平台，需要添加 opkg 参数 ```arch mipsel_1004kc_dsp 500```

```
src/gz xretia_1004kc https://gitlab.com/dfc643/openwrt-repo/-/raw/master/mipsel_1004kc_dsp
```

同时在软件包签名选项签名加上注释符号```#```。

```
#option check_signature
```

然后执行软件包更新命令。

```
opkg update
```

## 版权说明

所有修改包著作权归原作者所有，所有原创包著作权归 @dfc643 及其朋友所有。

**二次、三次、N 次修改请注明来源！**    
**别名说明：** xRetia 是 @dfc643 的别名。

(c) 2020 xRetia.

&nbsp;


# openwrt-repo

Some useful OpenWRT package modified or create by @dfc643 or its friends.

## Before Update Package

Please make sure following packages is installed in your OpenWRT devices, due to this repo hosted on GitLab only support https protocol.

```
opkg install wget ca-certificates
```

## How to Use?

Append following line to "Packges > Settings > Custom Source" and **Apply & Save**.

```
src/gz xretia_repo https://gitlab.com/dfc643/openwrt-repo/-/raw/master
```

For MediaTek MT762x Platform (add the line ```arch mipsel_1004kc_dsp 500``` to opkg configuration first)

```
src/gz xretia_1004kc https://gitlab.com/dfc643/openwrt-repo/-/raw/master/mipsel_1004kc_dsp
```

Add a comment symbol ```#``` before the package check signature option.

```
#option check_signature
```

Then update your package list by command

```
opkg update
```

## License

All modified package copyrights belong to the original author, and all original package copyrights belong to @ dfc643 and its friends.

**Please indicate the source of the second modification.**    
**Alias description:** xRetia is an alias for @dfc643.

(c) 2020 xRetia.